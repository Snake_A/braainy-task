import { ProductListComponent } from './components/product/product-list/product-list.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { environment } from '../environments/environment';
import { ContactListComponent } from './components/contacts/contact-list/contact-list.component';

const routes: Routes = [
  { path: '', redirectTo: `${environment.productRoute}`, pathMatch: 'full'},
  { path: environment.contactsRoute, component: ContactListComponent, pathMatch: 'full' },
  { path: environment.productRoute, component: ProductListComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
