import { EnumToArrayPipe } from './../pipes/enum-to-array.pipe';
import { ButtonComponent } from './button/button.component';
import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { DropdownComponent } from './dropdown/dropdown.component';
import { InputComponent } from './input/input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        ButtonComponent,
        DropdownComponent,
        InputComponent,
        EnumToArrayPipe,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
    ],
    exports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        ButtonComponent,
        DropdownComponent,
        InputComponent,
        EnumToArrayPipe,
    ],
    providers: []
})
export class SharedModule { }