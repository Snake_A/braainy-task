import { ButtonStyle } from './../button/button.component';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public readonly ButtonStyle = ButtonStyle;

  constructor() { }

  ngOnInit(): void {
  }

}
