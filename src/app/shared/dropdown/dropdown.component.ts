import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent implements OnInit {

  @Input() public placeholder: string = '';
  @Input() public options: string[] = [];
  @Input() public controlName: string = '';
  @Input() public groupName: FormGroup = new FormGroup({});

  constructor() { }

  ngOnInit(): void {
  }

}
