import { Component, Input, OnInit } from '@angular/core';

export enum ButtonStyle {
  primaryButton,
  warningButton,
  dangerButton,
}

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  @Input() public label: string = '';
  @Input() public disabled: boolean = false;
  @Input() public btnStyle: ButtonStyle | undefined;

  public readonly ButtonStyle = ButtonStyle;

  constructor() { }

  ngOnInit(): void {
  }

}
