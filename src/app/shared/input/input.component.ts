import { FormGroup } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  @Input() public type: string = '';
  @Input() public placeholder: string = '';
  @Input() public controlName: string = '';
  @Input() public groupName: FormGroup = new FormGroup({});

  constructor() { }

  ngOnInit(): void {
  }

}
