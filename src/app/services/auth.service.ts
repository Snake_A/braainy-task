import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class AuthService {
    private readonly token$$ = new BehaviorSubject<string>('');
    public readonly token$ = this.token$$;
    private readonly authToken = '749f6c0f873eb98f16257eec9baa47c944617d34';

    constructor() {
        this.setToken();
    }

    private setToken() {
        this.token$$.next(this.authToken);
    }

}