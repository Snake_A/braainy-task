import { IContact } from './../interfaces/contacts.interface';
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { environment } from "../../environments/environment";

@Injectable()
export class ContactsService {
    private readonly apiRoute = environment.apiBaseRoute;

    private readonly contacts$$: BehaviorSubject<IContact[]> = new BehaviorSubject<IContact[]>([]);
    public readonly contacts$: Observable<IContact[]> = this.contacts$$;

    constructor(private readonly httpClientService: HttpClient) {
        this.getAllContacts();
    }

    private getAllContacts() {
        const path = `${this.apiRoute}/${environment.contactsRoute}`;

        return this.httpClientService.get(path);
    }
}