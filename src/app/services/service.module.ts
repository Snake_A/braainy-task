import { ProductsService } from './products.service';
import { AuthService } from './auth.service';
import { ContactsService } from './contacts.service';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [HttpClientModule],
    exports: [HttpClientModule],
    providers: [
        ProductsService,
        ContactsService,
        AuthService,
    ]
})
export class ServiceModule { }