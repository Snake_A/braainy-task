import { IProducts } from './../interfaces/products.interface';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class ProductsService {
    private readonly apiRoute = environment.apiBaseRoute;

    private readonly products$$ = new BehaviorSubject<IProducts[]>([]);
    public readonly products$: Observable<IProducts[]> = this.products$$.asObservable();

    constructor(private readonly httpClient: HttpClient) {
        this.getAllProducts();
    }

    private getAllProducts() {
        const path = `${this.apiRoute}/${environment.productRoute}`;
        return this.httpClient.get(path);
    }
}