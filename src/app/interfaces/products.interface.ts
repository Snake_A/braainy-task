export interface IProducts {
    organization: any;
    name: string;
    description: string;
    account: any;
    productNo: string;
    suppliersProductNo: string;
    salesTaxRuleset: any;
    isArchived: boolean;
    prices: any;
}