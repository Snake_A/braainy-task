export interface IContact {
    type: any;
    organization: any
    createdTime: Date;
    name: string;
    street: string;
    cityText: string;
    state: any;
    stateText: string;
    zipcode: any;
    zipcodeText: string;
    contactNo: string;
    phone: string;
    fax: string;
    currency: any;
    registrationNo: string;
    ean: string;
    locale: any;
    isCustomer: boolean;
    isSupplier: boolean;
    paymentTermsMode: any;
    paymentTermsDays: number;
    contactPersons: any;
    accessCode: any;
    emailAttachmentDeliveryMode: any;
    isArchived: boolean;
}