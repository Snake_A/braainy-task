import { EnumToArrayPipe } from './pipes/enum-to-array.pipe';
import { ComponentsModule } from './components/components.module';
import { ButtonComponent } from './shared/button/button.component';
import { SharedModule } from './shared/shared.module';
import { ServiceModule } from './services/service.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
  ],
  imports: [
    ComponentsModule,
    SharedModule,
    ServiceModule,
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
