import { SharedModule } from './../shared/shared.module';
import { ContactDetailsComponent } from './contacts/contact-details/contact-details.component';
import { ProductDetailsComponent } from './product/product-details/product-details.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { NgModule } from "@angular/core";
import { ContactListComponent } from './contacts/contact-list/contact-list.component';

@NgModule({
    declarations: [
        ProductListComponent,
        ProductDetailsComponent,
        ContactListComponent,
        ContactDetailsComponent,
    ],
    imports: [
        SharedModule,
    ],
    exports: [],
    providers: []
})
export class ComponentsModule {}