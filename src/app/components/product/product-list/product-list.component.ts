import { CurrencyEnum } from './../../../enums/currency.enum';
import { ButtonStyle } from './../../../shared/button/button.component';
import { ProductsService } from './../../../services/products.service';
import { Observable, ReplaySubject, Subscription } from 'rxjs';
import { filter, tap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { IProducts } from '../../../interfaces/products.interface';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  // Enums
  public readonly ButtonStyle = ButtonStyle;
  public readonly sortOptions = ['Ascending', 'Descending'];
  public readonly Currencies = CurrencyEnum;

  // Data
  private readonly products$$: ReplaySubject<IProducts[]> = new ReplaySubject<IProducts[]>(1);
  public readonly products$: Observable<IProducts[]> = this.products$$.asObservable();

  // Reactive forms
  public productForm: FormGroup;

  private readonly subscriptions: Subscription = new Subscription;


  constructor(
    private readonly productsService: ProductsService,
    private readonly formBuilder: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.subscriptions.add(
      this.productsService.products$
        .pipe(
          filter(products => products != null),
          tap(products => console.log(products))
        )
        .subscribe(products => {
          this.products$$.next(products)
        })
    )

    this.initProductForm();
    console.log(this.productForm);
    
  }

  private initProductForm() {
    this.productForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      organization: ['', [Validators.required]],
      account: ['', [Validators.required]],
      price: ['', [Validators.required]],
      currency: ['', [Validators.required]]
    });
  }

  public clickEvent() {
    console.log('clicked');
    
  }


}
