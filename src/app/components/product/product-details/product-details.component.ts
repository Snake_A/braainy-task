import { IProducts } from './../../../interfaces/products.interface';
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  @Input()
  public product: IProducts = {} as IProducts;

  constructor() { }

  ngOnInit(): void {

  }

}
